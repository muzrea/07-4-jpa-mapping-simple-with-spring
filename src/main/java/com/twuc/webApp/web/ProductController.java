package com.twuc.webApp.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.swing.text.html.parser.Entity;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ProductController {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private EntityManager em;

    private ObjectMapper objectMapper;

    @PostMapping("/products")
    public ResponseEntity<Product> postProduct(@RequestBody @Valid CreateProductRequest productRequest) {
        Product product = productRepository.save(new Product(productRequest.getName(), productRequest.getPrice(), productRequest.getUnit()));
        productRepository.flush();
        em.clear();
        StringBuilder location = new StringBuilder();
        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header("location",location.append("http://localhost/api/products/").append(product.getId()).toString())
                .build();
    }

    @GetMapping("/products/{productId}")
    public ResponseEntity<Product> getProduct(@PathVariable Long productId){
        Optional<Product> product = productRepository.findById(productId);
        if(!product.isPresent()){
            return ResponseEntity.status(404).build();
        }
        return ResponseEntity.status(200)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(product.get());
    }
}